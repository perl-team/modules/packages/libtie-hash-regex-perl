libtie-hash-regex-perl (1.14-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtie-hash-regex-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 15 Oct 2022 17:59:51 +0100

libtie-hash-regex-perl (1.14-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 1.14.
  * Declare compliance with Debian Policy 4.5.1.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 Jan 2021 20:41:07 +0100

libtie-hash-regex-perl (1.13-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * Import upstream version 1.13.
  * Add debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.

 -- gregor herrmann <gregoa@debian.org>  Sat, 07 Nov 2020 16:48:21 +0100

libtie-hash-regex-perl (1.12-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: refresh license blurbs.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.

 -- gregor herrmann <gregoa@debian.org>  Thu, 19 Apr 2018 21:06:56 +0200

libtie-hash-regex-perl (1.02-2) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * debian/control: un-wrap Vcs-Browser fields.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.
  * Remove Andres Mejia from Uploaders on request of the MIA team.
    (Closes: #743560)

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Niko Tyni ]
  * Add myself to Uploaders
  * Declare the package autopkgtestable
  * Add explicit build dependency on libmodule-build-perl
  * Update to Standards-Version 3.9.6
  * Change to debhelper v9 tiny debian/rules
  * Change to dpkg source format 3.0 (quilt)

 -- Niko Tyni <ntyni@debian.org>  Fri, 05 Jun 2015 21:25:22 +0300

libtie-hash-regex-perl (1.02-1) unstable; urgency=low

  * Initial release. (Closes: #510144)

 -- Andres Mejia <mcitadel@gmail.com>  Mon, 29 Dec 2008 14:07:04 -0500
